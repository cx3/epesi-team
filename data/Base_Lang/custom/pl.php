<?php
global $custom_translations;
$custom_translations['Allow view record(s)']='';
$custom_translations['Allow view all record(s)']='';
$custom_translations['Allow edit record(s)']='';
$custom_translations['Allow edit all record(s)']='';
$custom_translations['Allow print record(s)']='';
$custom_translations['Allow print all record(s)']='';
$custom_translations['Allow delete record(s)']='';
$custom_translations['Allow delete all record(s)']='';
$custom_translations['Shoutbox Admin']='';
$custom_translations['Notify about shoutbox messages']='';
$custom_translations['no']='';
$custom_translations['only personal messages']='';
$custom_translations['personal and public messages']='';
$custom_translations['Additional title']='';
$custom_translations['You are in field']='';
$custom_translations['Employee or Customer']='';
$custom_translations['---']='';
$custom_translations['Advanced Filter']='';
